﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBoss : MonoBehaviour
{
    public enum State { Idle, Run, Attack, Damage, Death };
    public State state;

    public GameObject prefab;
    public GameObject prefab2;
    public GameObject prefab3;
    public GameObject prefab4;
    public GameObject prefab5;

    NavMeshAgent agent;
    private Animator anim;
    private SoundPlayer sound;

    [Header("Enemy properties")]
    public int life = 5;

    [Header("Target Detection")]
    public float radius;
    public float idleRadius;
    public float chaseRadius;
    public LayerMask targetMask;
    public bool targetDetected = false;
    private Transform targetTransform;

    [Header("Patrol path")]
    public bool stopAtEachNode = true;
    public float timeStopped = 1.0f;
    private float timeCounter = 0.0f;
    public GameObject[] points;
    private int currentNode = 0;
    private bool nearNode = false;

    [Header("Explosion properties")]
    public float explodeDistance;
    public float explosionRadius;
    public float explosionForce;
    public ParticleSystem explosionPS;

    // Use this for initialization
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        int d = Random.Range(0, points.Length);
        agent.SetDestination(points[d].transform.position);

        anim = GetComponentInChildren<Animator>();
        sound = GetComponentInChildren<SoundPlayer>();

        nearNode = true;
        SetIdle();
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Idle:
                Idle();
                break;
            case State.Run:
                Patrol();
                break;
            case State.Attack:
                Chase();
                break;
            case State.Damage:
                Explode();
                break;
            case State.Death:
                Dead();
                break;
            default:
                break;
        }

        if (agent.remainingDistance < 0.5)
        {

            int d = Random.Range(0, points.Length);
            agent.SetDestination(points[d].transform.position);

        }
        
    }
    private void FixedUpdate()
    {
        targetDetected = false;
        Collider[] cols = Physics.OverlapSphere(this.transform.position, radius, targetMask);
        if (cols.Length != 0)
        {
            targetDetected = true;
            targetTransform = cols[0].transform;
        }
    }

    void Idle()
    {
        if (targetDetected)
        {
            //SetAttack();
            return;
        }

        if (timeCounter >= timeStopped)
        {
            //if (nearNode) GoNearNode();
            //else GoNextNode();
            //SetPatrol();
        }
        else timeCounter += Time.deltaTime;
    }
    void Patrol()
    {
        if (targetDetected)
        {
            //SetAttack();
            return;
        }

        
    }
    void Chase()
    {
        if (!targetDetected)
        {
            nearNode = true;
            SetIdle();
            return;
        }

        if (Vector3.Distance(transform.position, targetTransform.position) <= explodeDistance)
        {
            SetDamage();
        }


    }
    void Explode() { }
    void Dead() { }

    void SetIdle()
    {
        //anim.SetBool("isMoving", false);
        radius = idleRadius;
        timeCounter = 0;

        int random = Random.Range(0, 4);
        //sound.Play(random, 1);

        state = State.Idle;
    }
    void SetRun()
    {
        
        radius = idleRadius;
        anim.SetBool("isMoving", true);

        state = State.Run;
    }
    /*void SetAttack()
    {
        anim.SetBool("isMoving", true);
        radius = chaseRadius;

        state = State.Attack;
    }*/
    void SetDamage()
    {
        sound.Play(9, 1);
        anim.SetTrigger("Damage");

        state = State.Damage;
    }
    void SetDeath()
    {
        //sound.Play(8, 1);
        Destroy(this.gameObject);
        state = State.Death;

        prefab.name = "Barrera5";
        Destroy(prefab);

        prefab2.name = "Barrera5_2";
        Destroy(prefab2);

        prefab3.name = "Barrera5_3";
        Destroy(prefab3);

        prefab4.name = "Barrera5_4";
        Destroy(prefab4);

        prefab5.name = "Barrera5_5";
        Destroy(prefab5);

    }

    public void Explosion()
    {
        Collider[] cols = Physics.OverlapSphere(this.transform.position, explosionRadius);
        foreach (Collider c in cols)
        {
            if (c.attachedRigidbody != null)
            {
                c.attachedRigidbody.AddExplosionForce(explosionForce, this.transform.position, explosionRadius, 1, ForceMode.Impulse);
            }
        }
        int random = Random.Range(4, 8);
        sound.Play(random, 1);
        explosionPS.Play();
        explosionPS.transform.parent = null;

        SetDeath();
    }
    private void OnDrawGizmos()
    {
        Color color = Color.green;
        if (targetDetected) color = Color.red;
        color.a = 0.1f;
        Gizmos.color = color;
        Gizmos.DrawSphere(this.transform.position, radius);
    }

    public void Damage(int hit)
    {
        Debug.Log("Enemy damage");
        if (state == State.Death) return;
        life -= hit;
        if (life <= 0) SetDeath();
    }

    void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.tag == "Shot")
        {
            Damage(5);
        }
    }
}
