﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour

{
 public int startingHealth = 100;
public int currentHealth;
public Image damageImage;
public AudioClip deathClip;
public float flashSpeed = 5f;
public Color flashColour = new Color(1f, 0f, 0f, 0.1f);

AudioSource playerAudio;
PlayerMovement playerMovement;
bool isDead;
bool damaged;


    void Awake()
{
    playerAudio = GetComponent<AudioSource>();
    playerMovement = GetComponent<PlayerMovement>();
    currentHealth = startingHealth;
}
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        if (damaged)
    {
        damageImage.color = flashColour;
    }
    else
    {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
    }
    damaged = false;
    }

public void TakeDamage (int amount)
{
    damaged = true;
    currentHealth -= amount;
    //playerAudio.Play();
    if(currentHealth <= 0)
    {
        SceneManager.LoadScene("GameOverScene");
    }
}

    /*void Death()
    {
        isDead = true;

        playerAudio.clip = deathClip;
        playerAudio.Play();

        playerMovement.enabled = false;

    }
    public void RestartLevel()
    {
        Application.LoadLevel (Application.loadedLevel);

    }*/

    void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.tag == "WinHouse")
        {
            SceneManager.LoadScene("YouWinScene");
        }
    }


}
