﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleManager : MonoBehaviour
{

    public AudioSource buttonA;

    public void PulsaPlay()
    {
        buttonA.Play();
        SceneManager.LoadScene("GameplayScene");
    }

    public void PulsaCredits()
    {
        buttonA.Play();
        SceneManager.LoadScene("CreditsScene");
    }

    public void PulsaExit()
    {
        buttonA.Play();
        Application.Quit();
    }
}

