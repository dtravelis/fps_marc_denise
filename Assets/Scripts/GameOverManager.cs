﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{

    public AudioSource buttonA;

    public void PulsaRetry()
    {
        buttonA.Play();
        SceneManager.LoadScene("GameplayScene");
    }

    public void PulsaMenu()
    {
        buttonA.Play();
        SceneManager.LoadScene("TitleScene");
    }

    public void PulsaExit()
    {
        buttonA.Play();
        Application.Quit();
    }
}

